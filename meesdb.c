/* license is to be decided, copyright m.aep@live.com */
// TODO close pots on exit
#define _POSIX_C_SOURCE 200809L // dprintf
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <chromaprint.h>

static_assert(CHAR_BIT == 8, "");

enum {
    READBUF_SIZE    = 0x4000,           // maximum size of input cmd
    MATCH_OFFSET    = 128,              // ? 
    MATCH_NEAR_LEN  = 12                // try match if fp has similar length +-
};
static const float MATCH_LIMIT = 0.5f;  // chosen arbitraily

struct memmap {
    uint8_t*        ptr;
    int64_t         size;
    int             refcount;
};

struct database {
    int             fd;
    pthread_mutex_t mutex;
    struct memmap*  mem;
};

struct fingerprint {
    uint8_t*        buf;
    int32_t         length;
    uint16_t        size;
    uint8_t         crc;
};

struct match {
    struct memmap*  mem;
    int64_t         pos;
    int64_t         index;
    double          score;
};

static struct database database;    // needed by exit handler

static int imin(int a, int b) { return a < b ? a : b; }
//static int imax(int a, int b) { return a > b ? a : b; }

static struct memmap* acquire_mem(struct database* db)
{
    pthread_mutex_lock(&db->mutex);
    struct memmap* mem = db->mem;
    mem->refcount++;
    pthread_mutex_unlock(&db->mutex);
    return mem;
}

static void release_mem(struct database* db, struct memmap* mem)
{
    pthread_mutex_lock(&db->mutex);
    if (--mem->refcount == 0) {
        munmap(mem->ptr, mem->size);
        free(mem);
    }    
    pthread_mutex_unlock(&db->mutex);
}

// returns the number of bits that are not equal
static int diff_bits(const uint8_t* a, const uint8_t* b, int size)
{
    int biterr = 0;
    const uint64_t* a64 = (const uint64_t*)a;
    const uint64_t* b64 = (const uint64_t*)b;
    for (int i = 0; i < size / 8; i++)
        biterr += __builtin_popcountll(a64[i] ^ b64[i]);
    // TODO compare size % 8 bits at end
    return biterr;
}

// works by moving fingerprints over each other and keeping the best match
// this is still work in progress
static float match_fingerprints(const struct fingerprint* a, const struct fingerprint* b)
{
    double best = INT_MAX;
    if (imin(a->size, b->size) < MATCH_OFFSET)
        return 0;
    for (int i = 0; i < MATCH_OFFSET; i+= 4) {
        int size = imin(a->size - i, b->size);
        int bits = diff_bits(a->buf + i, b->buf, size);
        best = fmin(best, (double)bits / size);
    }
    for (int i = 4; i < MATCH_OFFSET; i+= 4) {
        int size = imin(a->size, b->size - i);
        int bits = diff_bits(a->buf, b->buf + i, size);
        best = fmin(best, (double)bits / size);
    }
    return 1. - best / 4.;
}

// calculate 8-bit crc
static uint8_t cacluate_crc8(const uint8_t* buf, int size)
{
    // https://chromium.googlesource.com/chromiumos/platform/vboot_reference/+/master/firmware/lib/crc8.c
    unsigned crc = 0;
    for (int i = 0; i < size; i++) {
        crc ^= buf[i] << 8;
        for(int k = 0; k < 8; k++) {
            if (crc & 0x8000)
                crc ^= 0x1070 << 3;
            crc <<= 1;
        }
    }
    return crc >> 8;
}

// fills fingerprint from buffer, returns read bytes or -1 or error
static int64_t read_fingerprint(uint8_t* buf, int64_t bufsize, struct fingerprint* fp)
{
    if (bufsize < 8 || buf[0] != 0xa5)
        return -1;
    fp->crc = buf[1];
    memcpy(&fp->size, buf + 2, 2);
    memcpy(&fp->length, buf + 4, 2);
    if (fp->size > bufsize - 8)
        return -1;
    fp->buf = buf + 8;
    return fp->size + 8;
}

// searches for a fingerprint match. if a match is found the function
// returns true. the index is stored in s.index and the match score in s.score.
// when the end is reached the function returns with false. on error the function
// returns false and m.pos != m.size
static bool match_next(struct match* m, const struct fingerprint* fp)
{
    struct fingerprint testfp = {};
    while (m->pos < m->mem->size) {
        int64_t bytes = read_fingerprint(m->mem->ptr + m->pos, m->mem->size - m->pos, &testfp);
        if (bytes == -1)
            return false;
        m->index = m->pos;
        m->pos  += bytes;
        if (fp->length && abs(testfp.length - fp->length) > MATCH_NEAR_LEN)
            continue;
        m->score = match_fingerprints(fp, &testfp);
        if (m->score > MATCH_LIMIT) // TODO make this a setting ?
            return true;
    }
    return false;
}

// appends a fingerprint to the file
static int64_t add_fingerprint(struct database* db, const struct fingerprint* fp)
{
    uint8_t buf[8 + fp->size];
    buf[0] = 0xa5;
    buf[1] = cacluate_crc8(fp->buf, fp->size);
    memcpy(buf + 2, &fp->size, 2);
    memcpy(buf + 4, &fp->length, 4);
    memcpy(buf + 8, fp->buf, fp->size);
    int64_t index = -1;
    
    pthread_mutex_lock(&db->mutex);
    if (write(db->fd, buf, sizeof buf) == sizeof buf) {
        index = db->mem->size;
        if (db->mem->refcount == 1) {
            munmap(db->mem->ptr, db->mem->size);
        } else {
            db->mem = calloc(1, sizeof *db->mem);
            db->mem->refcount = 1;
        }
        db->mem->size = index + sizeof buf;
        // db->mem->ptr = mmap(NULL, db->size, PROT_READ | PROT_WRITE, MAP_SHARED, db->fd, 0);
        db->mem->ptr = mmap(NULL, db->mem->size, PROT_READ, MAP_SHARED, db->fd, 0);
        assert(db->mem->ptr != MAP_FAILED);
        
    } else {
        fprintf(stderr, "write failed");
    }
    pthread_mutex_unlock(&db->mutex);
    
    return index;
}

// finds exact match of fingerprint
static int64_t find_fingerprint(struct database* db, const struct fingerprint* fp)
{
    struct fingerprint tmp = {};
    struct memmap* mem = acquire_mem(db);
    const int64_t size = mem->size;
    int64_t i = 0, bytes = 0;
    for (i = 1024; i < size; i += bytes) {
        bytes = read_fingerprint(mem->ptr + i, size - i, &tmp);
        if (bytes == -1)    // read error
            break;
        if (tmp.size == size && !memcmp(tmp.buf, fp->buf, tmp.size))
            break;          // found match
    }
    release_mem(db, mem);
    return (i == size || bytes == -1) ? -1 : i;
}

static void close_db(struct database* db)
{
    if (db->mem && db->mem->ptr)
        munmap(db->mem->ptr, db->mem->size);
    free(db->mem);
    if (db->fd != -1)
        close(db->fd);
}

// validates checksums of the entire database, not mt-capable
static bool validate_db(struct database* db)
{
    int64_t i = 0, bytes = 0;
    if (memcmp(db->mem->ptr, "rm191101", 8) == 0) {
        struct fingerprint fp = {};
        for (i = 1024; i < db->mem->size; i += bytes) {
            bytes = read_fingerprint(db->mem->ptr + i, db->mem->size - i, &fp);
            if (bytes == -1 || fp.crc != cacluate_crc8(fp.buf, fp.size))
                break;
        }
    }
    return i == db->mem->size;
}

// loads the database and validates file integrity
static bool load_db_(struct database* db, const char* path)
{
    db->fd = open(path, O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
    if (db->fd == -1)
        return false;

    if (flock(db->fd, LOCK_EX | LOCK_NB) == -1)
        return false;

    struct stat st = {};
    if (stat(path, &st) == -1) {
        close_db(db);
        return false;
    }
    assert(st.st_size < INT64_MAX);
    int64_t size = st.st_size;

    if (size == 0) { // write header on new file
        static const char tmp[1024] = "rm191101";
        size = sizeof tmp;
        if (write(db->fd, tmp, sizeof tmp) != sizeof tmp)
            return false;
    }

    db->mem = calloc(1, sizeof *db->mem);
    db->mem->size = size;
    db->mem->refcount = 1;
    db->mem->ptr = mmap(NULL, size, PROT_READ, MAP_SHARED, db->fd, 0);
    if (db->mem->ptr == MAP_FAILED)
        return false;
        
    return validate_db(db);
}

static bool load_db(struct database* db, const char* path)
{
    db->mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
    bool ok = load_db_(db, path);
    if (!ok)
        close_db(db);
    return ok;
}

static void zero_newline(char* str)
{
    for (; *str; str++)
        if (*str == '\n' || *str == '\r')
            *str = 0;
}

static char* find_keyval(char* str, const char* key)
{
    char* tmp = strstr(str, key);
    return tmp ? tmp + strlen(key) : NULL;
}

// str must be freed with chromaprint_dealloc()
static bool encode_fingerprint(const struct fingerprint* fp, char** str)
{
    int size = 0;
    if (!chromaprint_encode_fingerprint(fp->buf, fp->size / 4, CHROMAPRINT_ALGORITHM_DEFAULT,
                                        (void**)str, &size, 1)) {
        return false;
    }
    // str is zero terminated but it's not documented. so i check to be sure.
    assert((*str)[size] == 0);
    return true;
}

// fb->buf must be freed with chromaprint_dealloc()
static bool decode_fingerprint(char* str, struct fingerprint* fp)
{
    int fpsize = 0, algo = 0;
    if (!chromaprint_decode_fingerprint(str, strlen(str), (void**)&fp->buf,
                                        &fpsize, &algo, 1)) {
        return false;
    }
    if (algo != CHROMAPRINT_ALGORITHM_DEFAULT) {
        chromaprint_dealloc(fp->buf);
        return false;
    }
    fp->size = fpsize * 4;
    return true;
}

static bool match_cmd(struct database* db, char* cmd, int fd)
{
    char* fpstr  = find_keyval(cmd, "\nfingerprint=");
    char* lenstr = find_keyval(cmd, "\nlength=");
    zero_newline(cmd);
    if (!fpstr)
        return false;

    struct fingerprint fp = {.length = lenstr ? atol(lenstr) : 0};
    if (!decode_fingerprint(fpstr, &fp))
        return false;

    dprintf(fd, "ok\n");
    struct match match = {.mem = acquire_mem(db), .pos = 1024};
    while (match_next(&match, &fp))
        dprintf(fd, "index=%ld\nscore=%.2f\n", match.index, match.score);

    release_mem(db, match.mem);
    chromaprint_dealloc(fp.buf);
    return true;
}

static bool add_cmd(struct database* db, char* cmd, int fd)
{
    char* fpstr  = find_keyval(cmd, "\nfingerprint=");
    char* lenstr = find_keyval(cmd, "\nlength=");
    zero_newline(cmd);
    if (!fpstr)
        return false;

    struct fingerprint fp = {.length = lenstr ? atol(lenstr) : 0};
    if (!decode_fingerprint(fpstr, &fp))
        return false;

    int64_t index = add_fingerprint(db, &fp);
    chromaprint_dealloc(fp.buf);
    if (index == -1)
        return false;

    dprintf(fd, "ok\nindex=%ld\n", index);
    return true;
}

static bool get_cmd(struct database* db, char* cmd, int fd)
{
    char* indexstr = find_keyval(cmd, "\nindex=");
    zero_newline(cmd);
    if (!indexstr)
        return false;
      
    int64_t index = atoll(indexstr);
    struct fingerprint fp = {};
    struct memmap* mem = acquire_mem(db);
    int64_t read_size = index <= 0 || index >= mem->size ? 0 :
                        read_fingerprint(mem->ptr + index, mem->size - index, &fp);
    release_mem(db, mem);
    
    char* fpstr = NULL;
    if (read_size <= 0 || !encode_fingerprint(&fp, &fpstr))
        return false;

    dprintf(fd, "ok\nfingerprint=%s\nlength=%d\n", fpstr, fp.length);
    chromaprint_dealloc(fpstr);
    return true;
}

static bool find_cmd(struct database* db, char* cmd, int fd)
{
    char* fpstr = find_keyval(cmd, "\nfingerprint=");
    zero_newline(cmd);
    if (!fpstr)
        return false;

    struct fingerprint fp = {};
    if (!decode_fingerprint(fpstr, &fp))
        return false;

    long index = find_fingerprint(db, &fp);
    chromaprint_dealloc(fp.buf);
    dprintf(fd, "ok\nindex=%ld\n", index);
    return true;
}

static void command(struct database* db, char* cmd, int fd)
{
    bool ok = false;
    if (strncmp(cmd, "match\n", 5) == 0) {
        ok = match_cmd(db, cmd, fd);
    } else if (strncmp(cmd, "add\n", 4) == 0) {
        ok = add_cmd(db, cmd, fd);
    } else if (strncmp(cmd, "get\n", 4) == 0) {
        ok = get_cmd(db, cmd, fd);
    } else if (strncmp(cmd, "find\n", 5) == 0) {
        ok = find_cmd(db, cmd, fd);
    }
    if (!ok)
        dprintf(fd, "error\n");
}

static void exit_handler(void)
{
    close_db(&database);
}

// loop that reads commands from read_fd and writes reponse to write_fd
static void commandloop(struct database* db, int read_fd, int write_fd)
{
    ssize_t siz = 1;
    size_t  pos = 0;
    char    readbuf[READBUF_SIZE];
    while (1) {
        siz = read(read_fd, readbuf + pos, sizeof readbuf - pos);
        pos += siz;
        if (siz <= 0)
            break;
        if (pos > sizeof readbuf - 2) {
            fputs("input too large", stderr);
            exit(EXIT_FAILURE);
        }
        readbuf[pos] = 0;
        if (pos > 2 && memcmp(readbuf + pos - 2, ";\n", 2) == 0) {
            pos = 0;
            command(&database, readbuf, write_fd);
            dprintf(write_fd, ";\n");
        }
    }
}

void* commandloop_thread(void* arg)
{
    int fd = (intptr_t)arg;
    commandloop(&database, fd, fd);
    return NULL;
}

// opens socket for listening and returns file descriptor
static int open_socket(int port) 
{  
    char portstr[8] = {};
    if (snprintf(portstr, sizeof(portstr), "%d", port) < 0)
        return -1;
        
    int fd = 0;
    struct addrinfo* info = NULL;
    struct addrinfo hints = {.ai_family = AF_INET, .ai_socktype = SOCK_STREAM, .ai_flags = AI_PASSIVE};
    if (getaddrinfo("localhost", portstr, &hints, &info) != 0)
        return -1;
        
    for (struct addrinfo* ai = info; ai; ai = ai->ai_next) {
        fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        int o = 1; // allow re-use of existing socket, nice with frequent restarts
        setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &o, sizeof o); 
        if (fd < 0 || bind(fd, ai->ai_addr, ai->ai_addrlen) == 0)
            break;
    }
    freeaddrinfo(info);

    int err = listen(fd, 4);
    if (err != 0)
        close(fd);
    return err == 0 ? fd : -1;
}

void socketloop(int port)
{
    int sok = open_socket(port);
    if (sok == -1) {
        fputs("failed to open socket", stderr);
        exit(EXIT_FAILURE);
    }
    while (1) {
        int fd = accept(sok, NULL, NULL);
        if (fd == -1)
            break;
        pthread_t thread;
        pthread_create(&thread, NULL, commandloop_thread, (void*)(intptr_t)fd);
        pthread_detach(thread);
    }
    close(sok);
}

int main(int argc, char** argv)
{
    char c = 0;
    int port = 0;
    const char* dbfile = "meesd-fp.bin";
    while ((c = getopt(argc, argv, "hd:p:")) != -1) {
        if (c == '?' || c == 'h') {
            puts("meesdb engine v1\noptions:\n\t-d db file\n\t-p port");
            return EXIT_FAILURE;
        } else if (c == 'd') {
            dbfile = optarg;
        } else if (c == 'p') {
            port = atoi(optarg);
        }
    }

    if (!load_db(&database, dbfile)) {
        fputs("failed to load database file", stderr);
        return EXIT_FAILURE;
    }
    atexit(exit_handler);
    printf("ready\n");
    fflush(stdout);

    if (port) {
        socketloop(port);
    } else {
        commandloop(&database, STDIN_FILENO, STDOUT_FILENO);
    }
    
    pthread_exit(NULL);
}

