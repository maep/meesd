# module to facilitate crawling of sites
# example:
# import meescrawl as mc
# # use ftp crawl tool or provide your own
# tool = mc.FtpTool('ftp.scene.org')
# # provide server credentials server:port, user, password
# cred = mc.Cred('foobar.org:433', 2, 'perkele')
# # run the crawl
# mc.crawl(tool, cred)

# encountered quirks:
# scene.org has files that cause peer reset; use wget to fetch files
# archives are password protected; dummy pw 
# archives overwrite own files; force overwrite flag
# archives have brokwn file permissoins; chmod
# json/sqlite can't ansi; break link, replce with ?
# wget stalls when ftp data connection drops; curl

import sys, os, ftplib, collections, tempfile, shlex 
import shutil, httplib, json, time, socket, urllib2, signal
import subprocess as sp, os.path as op

# for modarchive it's a number...
Node = collections.namedtuple('Node', 'name size isfile isdir islink')
Link = collections.namedtuple('Link', 'name size url link')
Scan = collections.namedtuple('Scan', 'name length fingerprint')
Cred = collections.namedtuple('Cred', 'server site password')
uext = collections.defaultdict(int)
devnull = open(os.devnull, 'wb')
logfile = open('crawl-%.0f.log' % time.time(), 'w')

# blacklisted extensions
bext = ['.nfo', '.diz', '.txt', '.info', '.doc', '.html', '.htm', '.readme', 
        '.cpp', '.pas', '.java', '.exe', '.com', '.dll', '.class', '.bat', 
        '.raw', '.dat', '.wob', '.bin', '.data', '.pix', '.pal', '.gfx', 
        '.bmp', '.tga', '.jpg', '.png', '.gif', '.lbm', '.pcx', '.iff',
        '.tif', '.lwo', '.lws', '.3ds', '.h', '.hpp', '.bak', '.asm', '.psd',
        '.log', '.c', '.ini', '.ds_store', '.py', '.pyc', '.xml', '.js',
        '.pdf', '.d64', '.mm', '.jpeg', '.prg', '.db', '.ico', '.so', '.svg',
        '.css', '.hh', '.cc', '.obj', '.lua', '.a']
# music extensions
mext = ['.mp3', '.ogg', '.ogm', '.flac', '.m4a', '.wma', '.wav', '.mod', 
        '.s3m', '.xm', '.it', '.mtm', '.umx', '.fst', '.ft', '.mo3',
        '.mp4', '.avi', '.mpg', '.mpeg', '.wmv', '.mov', '.mkv']
# amiga prefix extensinos
aext = ['mod.', 's3m.', 'xm.', 'it.', 'mtm.', 'umx.', 'fst.', 'ft.', 
        'mo3.', 'wav.']
# zip file extensions and unpack commands
zext = {'.zip'    :'unzip -oP "" "{IN}" -d {OUT}', 
        '.jar'    :'unzip -o "{IN}" -d {OUT}',
        '.rar'    :'unrar -f "{IN}" {OUT}', 
        '.lha'    :'lha xfw={OUT} "{IN}"',
        '.arj'    :'arj e -y "{IN}" {OUT}/',
        '.7z'     :'7zr x -yo{OUT} "{IN}"',
        '.tgz'    :'tar xf "{IN}" -C {OUT}',
        '.tbz2'   :'tar xf "{IN}" -C {OUT}',
        '.tar.gz' :'tar xf "{IN}" -C {OUT}',
        '.tar.bz2':'tar xf "{IN}" -C {OUT}',
        '.tar.xz' :'tar xf "{IN}" -C {OUT}',
        '.gz'     :'gunzip -f "{IN}"',  # needs to come after .tar.* exts
        '.bz2'    :'bunzip2 -f "{IN}"'}

def fixenc(s):
    try:
        s.decode('utf8')
        return s
    except UnicodeDecodeError:
        buf = ''
        for a in s:
            buf += a if ord(a) < 128 else '?'
        return buf

def log(*args):
    s = ' '.join(map(str, args)) + '\n'
    sys.stdout.write(s)
    sys.stdout.flush()
    logfile.write(s)
    logfile.flush()

def value(it, key):
    return next((i[len(key):] for i in it if i.startswith(key)), None)
    
def checkext(path, sfx, pfx=[]):
    uext[op.splitext(path)[1].lower()] += 1
    lp = path.lower()
    return any(lp.endswith(e) for e in sfx) or any(lp.startswith(e) for e in pfx)

def dscan(path):
    p = sp.Popen(['dscan', '-c', path], stdout=sp.PIPE)
    o = p.communicate()[0].splitlines()
    return value(o, 'acoustid:'), value(o, 'length:')    
   
def unpack(path):
    dst = op.join(op.dirname(path), 'files')
    tmp = next(zext[e] for e in zext if path.lower().endswith(e))
    cmd = shlex.split(tmp.format(IN=path, OUT=dst))
    os.makedirs(dst)
    rc  = sp.call(cmd, stdout=devnull, stderr=devnull)
    rc |= sp.call(['chmod', 'u+rwx', '-R', dst], stdout=devnull, stderr=devnull)
    return rc == 0

def wget(url, dst):
    log('wget     ', url)
    cmd = ['curl', url, '-o', dst]
    return sp.call(cmd, stdout=devnull, stderr=devnull) == 0

def walk(tool, root, path, skip):
    tool.cd(root + path)
    files = []
    dirs  = []
    for n in tool.nodes():
        if n.isdir: dirs.append(n.name)
        if n.isfile: files.append(n)
    if skip and skip[0] in dirs:
        dirs = dirs[dirs.index(skip[0]):]
    else:
        for f in files:
            link = path + '/' + f.name
            yield Link(f.name, f.size, tool.url(link), link)
    for d in dirs:
        for link in walk(tool, root, path + '/' + d, skip[1:]):
            yield link

class RangeTool(object):
    def __init__(self, geturl):
        self.geturl = geturl
    def get(self, link, path):
        return wget(link.url, path)
    def crawl(self, range_, skip):
        for i in xrange(*map(int, range_.split(':'))):
            url, name = self.geturl(i)
            if url:
                yield Link(name, 0, url, str(i))

class FtpTool(object):
    def __init__(self, server, root=''):
        self.server = server
        self.root   = root
    def login(self):
        self.ftp = ftplib.FTP(self.server)
        self.ftp.login()
    def cd(self, dir_):
        try:
            self.ftp.cwd(dir_)
        except:
            self.login()
            self.ftp.cwd(dir_)
    def nodes(self):
        buf = []
        self.ftp.dir(buf.append)
        return [Node(l.split()[8], int(l.split()[4]), l[0] == '-', l[0] == 'd', l[0] == 'l') for l in buf]
    def get(self, link, path):
        return wget(self.url(link), path)
    def crawl(self, path, skip):
        self.login()
        return walk(self, root, path, skip.split('/'))
    def url(self, link):
        return 'ftp://' + self.server + self.root + link
    
def process(tool, link, conf):
    mext_   = conf.get('mext', mext)
    aext_   = conf.get('aext', aext)
    maxsize = conf.get('maxsize', 0x1f400000)
    minlen  = conf.get('minlength', 0)
    tempdir = conf.get('tempdir', None) 
    if not checkext(link.name, mext_ + list(zext)):
        log('skip ext ', link.name)
        return
    if link.size and link.size > maxsize:
        log('skip size', link.link)
        return
    tmpdir = tempfile.mkdtemp(dir=tempdir)
    dst = op.join(tmpdir, link.name)
    if not tool.get(link, dst):
        log('error get', link.url)
    if checkext(dst, zext) and not unpack(dst):
        log('error unp', link.name)
    for root, dirs, files in os.walk(tmpdir):
        for f in files:
            if checkext(f, mext_, aext_):
                a, l = dscan(op.join(root, f))
                if a and l and float(l) >= minlen:
                    yield Scan(f, l, a)
                else:
                    log('skip len ', f)
            else:
                log('skip ext ', f)
    shutil.rmtree(tmpdir)

def addfingerprint(scan, link, cred):
    args = {'fingerprint':scan.fingerprint, 'length':scan.length, 'site':cred.site, 
            'link':fixenc(link.link), 'password':cred.password}
    c = httplib.HTTPConnection(cred.server)
    c.request('POST', '/meesd/api/add', json.dumps(args))
    r = c.getresponse()
    if r.status != 200:
        log('error:', r.status, r.reason)
    c.close()

# kwargs:
# nice      time between scans (0s)
# report    show report of unrecognized file extensions (True)
# mext      music file extensions
# aext      amiga music file prefix extensions
# maxsize   maximun download size (500 MiB)
# minlen    minimum song lengths (0s)
# tempdir   temp data dir (/tmp)
def crawl(tool, cred, **kwargs):
    root = sys.argv[1]   
    skip = (sys.argv + [''])[2]
    nice = kwargs.get('nice', None)
    start = time.clock()
    for link in tool.crawl(root, skip):
        if signal.sigint:
            sys.exit(0)
        for scan in process(tool, link, kwargs):
            log('process  ', scan.name, scan.length, link.link)
            addfingerprint(scan, link, cred)
        if nice:
            time.sleep(nice - time.clock() + start)
        start = time.clock()
    if kwargs.get('report', True):
        uk1 = set(uext) - set(bext + kwargs.get('mext', mext) + list(zext))
        uk2 = sorted((uext[e], e[1:]) for e in uk1) 
        log(*['%s:%d' % (e, c) for c, e in uk2 if c > 1])
   
def sighandler(signum, frame):
    signal.sigint = True

signal.sigint = False
signal.signal(signal.SIGINT, sighandler)
