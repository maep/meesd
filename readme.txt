
This is a small acoustic fingerprint database server. Written in Python2 and C11.

public web api:
    The api is accessed using http push. Available funcions:
    /meesd/api/add
    /meesd/api/get
    /meesd/api/match
    /meesd/api/sites
    [docs will be added]

Below is documentation of internal stuff. Igonre if you're not interested in internals.

meesdb protocol:
    commands: add, get, match, find
    key-value arguments must have no whitespaces near =. all commands are terminated by \n;\n
    * fields are optional, in the future, errors might provide a message 

    add                         add fingerprint to db
    fingerprint=<base64>       fingerprint genrated by cromaprint
    length=<int>                total song length in seconds
    -----------------
    ok
    index=<int>                 64 bit index at which the fingerpint is stored
    -----------------
    error

    get                         get fingerprint from db
    index=<int>                 index of fingerprint
    -----------------
    ok
    fingerprint=<base64>        base64
    length=<int>*               integer
    -----------------
    error

    match                       search the db for matching fingerprints
    fingerprint=<base64>        genrated by cromaprint
    length=<int>*               total song length in seconds, if > 0 only prints of 
    -----------------
    ok
    index=<int>*                for each result a index and a score are returned
    score=<int>*                lower score means better match. 0 = binary identical fp
    [...]
    -----------------
    error

    find                        search the db for identical fingerprint
    fingerprint=<base64>        genrated by cromaprint
    -----------------
    ok
    index=<int>                 index of first match or -1 if not in db
    -----------------
    error

File layout:
    The data is stored in a flat file consisting of a header and entries.
    For now, native endianess is used, maybe little endian in the future.
    Removing entries is considered but requires additional work.

header:
    file id         6 bytes     "rm1911"
    file version    2 bytes     "01"
    reserverd       1016 bytes

entry:
    magic byte      uint8       hex 0xa5
    crc-8           uint8       checksum of the fingerprint
    size            uint16      size of the fingerprint in bytes
    length          int32       length of the song in seconds
    fingerprint     n           raw uncompressed fingerprint

