#!/usr/bin/python2 -B
import os, sys, sqlite3, json, socket, collections, time, threading, traceback
import threading, signal, subprocess as sp, BaseHTTPServer, SimpleHTTPServer

fpdb   = None
metadb = None

def value(lst, key, default=None):
    return next((s[len(key):] for s in lst if s.startswith(key)), default)

def writecmd(s, buf):
    s.send(buf + '\n;\n')

def readcmd(s):
    buf = ''
    while not (buf and buf.endswith(';\n')):
        buf += s.recv(1024)
    return buf[:-2]

def meescmd(*arg):
    writecmd(fpdb.socket, '\n'.join(arg))
    return readcmd(fpdb.socket).splitlines()

def nlist(obj):
    return list(obj) if obj else []
 
def open_fingerprint_db(path):
    MeesDB = collections.namedtuple('MeesDB', 'process socket')
    p = sp.Popen(['./meesdb', '-p61440', '-d' + path], stdin=sp.PIPE, stdout=sp.PIPE)
    if p.stdout.read(6) != 'ready\n':
        raise Exception('failed to load db')  
    time.sleep(0.2) # wait a bit for the process to spin up
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('localhost', 61440)) 
    return MeesDB(p, s)

def open_meta_db(path):
    db = sqlite3.connect(path)
    c = db.cursor()
    c.execute('create table if not exists links (site, fpindex, link, ' \
              'unique(site, fpindex, link) on conflict ignore)')
    c.execute('create table if not exists sites (site, pass, name, url)')
    db.commit()
    return db

def select_password(site):
    c = metadb.cursor()
    c.execute('select pass from sites where site=?', [site])
    return next(iter(nlist(c.fetchone())), None)

def insert_link(site, index, link):
    c = metadb.cursor()
    c.execute('insert into links values (?,?,?)', [site, index, link])
    metadb.commit()

def select_link(index):
    c = metadb.cursor()
    c.execute('select sites.url, links.link from links inner join sites ' \
              'on links.site=sites.site where links.fpindex=?', [index])
    return [url.format(LINK=key) for url, key in nlist(c.fetchall())]

def select_index(site, link):
    c = metadb.cursor()
    c.execute('select fpindex from links where site=? and link=?', [site, link])
    return [row[0] for row in nlist(c.fetchall())]

def select_sites():
    c = metadb.cursor()
    c.execute('select site, name from sites')
    return nlist(c.fetchall())

def meesd_add(fingerprint, length):
    ret = meescmd('add', 'fingerprint=' + fingerprint, 'length=' + length)
    if ret[0] != 'ok':
        raise Exception('meesd add failed')
    return int(value(ret, 'index='))

def meesd_get(index):
    ret = meescmd('get', 'index=%d' % index)
    if ret[0] != 'ok':
        raise Exception('meesd get failed')
    return value(ret, 'fingerprint=')

def meesd_find(fingerprint):
    ret = meescmd('find', 'fingerprint=' + fingerprint)
    if ret[0] == 'error':
        raise Exception('meesd find returned error')
    return int(value(ret, 'index='))

def meesd_match(fingerprint, length, accuracy):
    ret = meescmd('match', 'fingerprint=' + fingerprint)
    if ret[0] == 'error':
        raise Exception('match returned error')
    for p in zip(ret[1::2], ret[2::2]):
        yield (float(value(p, 'score=')), int(value(p, 'index=')))

# returns nothing
def add(request, args):
    if not set(args) >= set(['fingerprint', 'link', 'site', 'password']):
        return 400, None
    if select_password(args['site']) != args['password']:
        return 401, None
    index = meesd_find(args['fingerprint'])
    if index == -1:
        index = meesd_add(args['fingerprint'], args.get('length', '0'))
    insert_link(int(args['site']), index, args['link'])
    return 200, None

# return list of [score, url]
def match(request, args):
    if not 'fingerprint' in args:
        return 400, None
    accuracy = args.get('accuracy', '0')
    if accuracy == '1':
        index = meesd_find(args['fingerprint'])
        m = [(1, index)] if index > 0 else None
    else:
        m = meesd_match(args['fingerprint'], args.get('length', '0'), accuracy)
    return 200, [(s, l) for s, i in nlist(m) for l in select_link(i)] 

# return list of bas64 encoded fingerprints
def get(request, args):
    if not set(args) >= set(['link', 'site']):
        return 400, None
    return 200, map(meesd_get, select_index(args['site'], args['link']))

# returns list of [site-id, name]
def sites(request, args):
    return 200, select_sites()

def handle_request(req):
    if not clisem.acquire(False):
        return 503, None
    handlers = {'/meesd/api/add':  add, 
                '/meesd/api/get':  get,
                '/meesd/api/match':match,
                '/meesd/api/sites':sites}
    try:
        if req.path not in handlers:
            return 405, None
        if not 'Content-length' in req.headers:
            return 411, None
        size = int(req.headers['Content-length'])
        if size > 0x4000:
            return 413, None
        args = json.loads(req.rfile.read(size))
        return handlers[req.path](req, args)
    except:
        traceback.print_exc()
        return 400, None
    finally:
        clisem.release()
    
class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_POST(self):
        status, cargo = handle_request(self)
        if status < 300:
            self.send_response(status)
            self.end_headers()
            self.wfile.write(json.dumps(cargo))
        else:
            self.send_error(status)

# quite nicely on ctrl-c            
def sigint(signo, frame):
    threading.Thread(target=server.shutdown).start()

signal.signal(signal.SIGINT, sigint)
clisem = threading.Semaphore(4)
fpdb   = open_fingerprint_db('meesd-fp.bin')
metadb = open_meta_db('meesd-meta.sqlite')
server = BaseHTTPServer.HTTPServer(('localhost', 1911), Handler)
print 'ready'
server.serve_forever()
