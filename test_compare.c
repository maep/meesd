static inline int imin(int a, int b) { return a < b ? a : b; }
static inline int imax(int a, int b) { return a > b ? a : b; }

// returns the number of bits that are not equal 
static int diff_bits(const uint8_t* a, const uint8_t* b, int size)
{
    int biterr = 0;
    const uint64_t* a64 = (const uint64_t*)a;
    const uint64_t* b64 = (const uint64_t*)b;
    for (int i = 0; i < size / 8; i++)
        biterr += __builtin_popcountll(a64[i] ^ b64[i]);
    // TODO compare size % 8 bits at end 
    return biterr;
}
 
// works by moving fingerprints over each other and keeping the best match
// this is still work in progress
static float match_fingerprints(const struct fingerprint* a, const struct fingerprint* b)
{
    float best = INT_MAX;
    if (imin(a->size, b->size) < MATCH_OFFSET)
        return 0;
    for (int i = 0; i < MATCH_OFFSET; i+= 4) {
        int size = imin(a->size - i, b->size);
        int bits = diff_bits(a->buf + i, b->buf, size);
        best = fminf(best, (float)bits / size);
    }
    for (int i = 4; i < MATCH_OFFSET; i+= 4) {
        int size = imin(a->size, b->size - i);
        int bits = diff_bits(a->buf, b->buf + i, size);
        best = fminf(best, (float)bits / size);
    }    
    return 1.f - best / 4.f;
}
