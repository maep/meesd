#!/usr/bin/python2 -B

import subprocess as sp, sys, os, argparse, httplib, contextlib, json, ssl

parser = argparse.ArgumentParser(description='meesdb client')
subprs = parser.add_subparsers(dest='command', help='commands')
parser.add_argument('-f', action='store_true', help='use fpcalc to generate fingerprint')
parser.add_argument('-s', dest='server', default='https://sakui.mooo.com', help='meesdb server, http(s)://server(:port)')
matchprs = subprs.add_parser('match',help='match fingerprint')
matchprs.add_argument('-e', action='store_true', help='look for exact match')
matchprs.add_argument('file', help='audio file to match')
matchprs.add_argument('site', nargs='?', default=0, type=int, help='limit search to site (0 for all), see "sites"')
getprs = subprs.add_parser('get', help='get fingerprint(s) for site link')
getprs.add_argument('site', type=int, help='site id, see "sites"')
getprs.add_argument('link', help='link/id/path of fingerprint')
sitespres = subprs.add_parser('sites', help='list site ids and names')
opts = parser.parse_args()

sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)

def value(key, heap):
    for line in heap:
        if line.startswith(key):
            return line[len(key):]
    return ''

def call(*args):
    p = sp.Popen(args, stdout=sp.PIPE)
    out = p.communicate()[0].splitlines()
    return [] if p.returncode else out

def scan(path):
    if not opts.f:
        try:
            out = call('dscan', '-c', path)
            return value('acoustid:', out), value('length:', out)
        except:
            print 'no dscan! trying fpcalc, which has a terrible module decoder'
            print 'get dscan from https://gitlab.com/maep/demosauce for best results'
    try:
        out = call('fpcalc', path)
        return value('FINGERPRINT=', out), value('DURATION=', out)
    except:
        sys.exit('no fpcalc')

def meescall(func, args=None):
    if opts.server.startswith('https://'):
        c = httplib.HTTPSConnection(opts.server[8:], context=sslcontext)
    elif opts.server.startswith('http://'):
        c = httplib.HTTPConnection(opts.server[7:])
    else:
        sys.exit('bad server protocoll')
    with contextlib.closing(c):
        c.request('POST', '/meesd/api/' + func, json.dumps(args))
        r = c.getresponse()
        if r.status != 200:
            sys.exit('meesdb api error %d, %s' % (r.status, r.reason))
        return json.loads(r.read())

def get():
    for fingerprint in meescall('get', {'site':opts.site, 'link':opts.link}):
        print fingerprint

def match():
    fingerprint, length = scan(opts.file)
    if not fingerprint:
        sys.exit('failed to get fingerprint from file')
    args = {'fingerprint':fingerprint, 'length':length, 'accuracy':int(opts.e)}
    for score, url in meescall('match', args):
        print '%1.1f' % score, url
    
def sites():
    for site, name in sorted(meescall('sites')):
        print '%3d' % site, name

{'get':get, 'match':match, 'sites':sites}[opts.command]()

