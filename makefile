include config.mk

PKGFLAGS = $(shell pkg-config --libs libchromaprint)

all: clean meesd

meesd:
	$(CC) $(CFLAGS) $(LDFLAGS) $(PKGFLAGS) meesdb.c -o meesdb

clean:
	rm -f meesdb
