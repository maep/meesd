#!/bin/sh
CC='gcc'

# remove old output files
rm -f config.mk

have_lib() {
    echo -n "checking for $1 ... "
    pkg-config "--exists" $1 2>/dev/null
    test $? -ne 0 && echo "no" && return 1
    echo "yes" && return 0
}

have_header() {
    echo -n "checking for $1 ... "
    echo "#include <$1>" | $CC $CFLAGS -E -xc -o /dev/null - 2>/dev/null
    test $? -ne 0 && echo "no" && return 1
    echo "yes" && return 0
}

assert_header() {
    have_header "$1" && return 0
    echo "missing header: $1" && exit 1
}

assert_lib() {
    have_lib "$1" && return 0 
    echo "missing lib: $1" && exit 1
}

assert_version() {
    pkg-config "--atleast-version=$2" "$1" && return 0
    echo "need $1 $2, have `pkg-config --modversion $1`" && exit 1
}

have_file() {
    echo -n "checking for $1 ... "
    test -e "$1" && echo "yes" && return 0
    echo "no" && return 1
}

have_exe() {
    echo -n "checking for $1 ... "
    which $1 >/dev/null 2>/dev/null
    test $? -ne 0 && echo "no" && return 1
    echo "yes" && return 0
}

have_cpuflag() {
    echo -n "checking for $1 ... "
    grep -q $1 "/proc/cpuinfo" 2>/dev/null
    test $? -ne 0 && echo "no" && return 1
    echo "yes" && return 0
}

assert_exe() {
   have_exe $1 && return 0
   echo "missing: $1" && exit 1
}

ask() {
    while true; do
        read -p "$1 [y/n] " reply
        case "$reply" in 
            Y|y) return 0;;
            N|n) return 1;;
        esac
    done
}

# in case there is local stuff
if test -d "/usr/local/include"; then CFLAGS="$CFLAGS -I/usr/local/include"; fi
if test -d '/usr/local/lib'; then LDFLAGS="$LDFLAGS -L/usr/local/lib"; fi

# build environment
assert_exe 'make'
assert_exe "$CC"
assert_exe "pkg-config"
assert_lib 'libchromaprint'

if have_cpuflag 'popcnt'; then
    CFLAGS="$CFLAGS -mpopcnt"
fi

if have_exe "ccache"; then
    CC="ccache $CC"
fi

# generate makefile
cat <<EOF >config.mk
CC          = $CC
CFLAGS      = -Wall -std=c11 -O2 -pthread $CFLAGS 
LDFLAGS     = -lm -pthread $LDFLAGS
EOF

echo "smooth operator"
